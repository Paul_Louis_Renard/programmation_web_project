-- --------------------------------------------------------

--
-- Structure de la table 'users'
--

CREATE TABLE IF NOT EXISTS Users (
  id int(11) NOT NULL,
  login varchar(255) NOT NULL,
  pass varchar(255) NOT NULL,
  persoQ varchar(255) NOT NULL,
  persoR varchar(255) NOT NULL,
  droit INT NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO Users (id, login, pass,persoQ,persoR,droit) VALUES
(1, 'root','root','Qu\'est-ce qui est jaune et qui attend ?','Jonathan',2),
(2, 'editor','edit','Quel est ma ville de naissance ?','Paris',1),
(3, 'Bertrand','Lannion1234','Quel est ma situation actuelle ?','étudiant',0),
(4, 'Béatrice','jesuisJesus','Quand me suis-je marié ?','18/04/1987',1),
(5, 'hooper','aloarat785','Quel est le nom de jeune fille de ma mère ?','Berange',0),
(6, 'herbez','ertgyh','Quel est mon prénom ?','Valentin',2),
(7, 'Mamaia','momo','Qu\'est-ce qui est jaune et qui attend ?','Jonathan',0);
