-- --------------------------------------------------------

--
-- Structure de la table 'Etudiant'
--

CREATE TABLE IF NOT EXISTS Etudiants (
  numetudiant varchar(255) NOT NULL,
  civilite varchar(255) NOT NULL,
  nom varchar(255) NOT NULL,
  prenom varchar(255) NOT NULL,
  dateNaissance varchar(255) NOT NULL,
  courrielPro varchar(255) NOT NULL,
  courrielPerso varchar(255),
  serieBac varchar(255),
  dateBac varchar(255),
  mentionBac varchar(255),
  diplome varchar(255),
  dateDiplome varchar(255),
  villeDiplome varchar(255),
  proprio varchar(255) NOT NULL,
  PRIMARY KEY (numetudiant)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO Etudiants (numetudiant, civilite, nom,prenom,dateNaissance,courrielPro,proprio) VALUES
('1','M','Renard','Paul-Louis','18/02/1997','prenard@enssat.fr','Dieu'),
('2','M','Pacquereau','Timothée','18/05/1997','timtim@enssat.fr','Dieu'),
('3','M','Herbez','Valentin','18/09/1997','vherbezd@enssat.fr','Dieu'),
('4','M','Trevidic','Goulven','18/02/1997','ad@enssat.fr','Dieu'),
('5','M','Thuieb','Thuebajs','18/02/1997','trd@enssat.fr','Dieu');


