<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ajout d'un étudiant</title>
</head>
<body>
<c:if test="${!empty msg}" >${msg} </c:if>
<form action="ajoutStd" method="POST">
numéro étudiant : <input type="text" name="numero" required>
<br>
<label for="civilite">Civilité :</label>
  <select id="civilite" name="civilite">
    <option value="M">M</option>
    <option value="Mme">Mme</option>
    <option value="NB">NB</option>
  </select>
<br>
prénom : <input type="text" name="prenom" required>
<br>
nom : <input type="text" name="nom" required>
<br>
date de naissance : <input type="text" name="date de naissance" required>
<br>
courriel pro : <input type="text" name="courriel pro" required>
<br>
courriel perso : <input type="text" name="courriel perso" >
<br>
série bac : <input type="text" name="serie bac" >
<br>
date bac : <input type="text" name="date bac" >
<br>
mention bac : <input type="text" name="mention bac" >
<br>
diplome : <input type="text" name="diplome" >
<br>
date diplome : <input type="text" name="date diplome">
<br>
ville diplome : <input type="text" name="ville diplome">
<br>
<input type="submit" value="valider">
</form>
<form action="options_Std" method="GET">
<input type="submit" value="retour">
</form>
</body>
</html>