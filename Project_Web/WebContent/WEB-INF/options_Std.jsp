<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des options</title>
</head>
<body>
	<form action="ajoutStd" method="POST">
		<input type="submit" value="ajout étudiant">
	<br>
	</form>
	<form action="deleteE" method="POST">
		 <input type="submit" value="supprimer étudiant">
	<br>
	</form>
	<form action="changeStd" method="POST">
		<input type="submit" value="modifier étudiant">
	<br>
	</form>
<c:choose>
	<c:when test="${ sessionScope.user.access == 0}">
	<form action="etudiantU" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when >
	<c:when test="${ sessionScope.user.access == 1}">

	<form action="etudiantA" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when>
	<c:when test="${ sessionScope.user.access == 2}">
	
	<form action="etudiantA" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when>
	
</c:choose>
</body>
</html>