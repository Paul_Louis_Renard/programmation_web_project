<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Supprimer un étudiant</title>
</head>
<body>
<form action="deleteE" method="POST">
<select name="sup">
<c:forEach var="entry" items="${requestScope['etudiants']}" >
	<option value="${entry.numetu }">${entry.numetu}, ${entry.nom}, ${entry.prénom }</option>
</c:forEach>
</select>
<br>
<input type="submit" value="Valider">
</form>
<form action="options_Std" method="POST">
<input type="submit" value="retour">
</form>
</body>
</html>