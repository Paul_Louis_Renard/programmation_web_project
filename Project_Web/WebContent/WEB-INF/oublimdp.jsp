<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Mot de passe oublié ?</title>
<c:if test="${! empty err }">${err} </c:if>
</head>
<body>
<form action="oubli" method="post">
<label for="pseudo">Login : </label>
<input type="text" id="pseudo" name="pseudo">
<input type="submit" value="Valider">

</form>
<c:if test="${ok1}">
Question personnelle : ${question}
<br>
<form action="oubli" method="post">
<label for="rperso">Réponse à la question personnelle :</label>
<input type="text" id="rperso" name="rperso">
<br>
<input type="submit" value="Valider">
</form>
</c:if>
<br>
<c:if test="${ok2}">
<br>
<form action="oubli" method="post">
<label for="pass">Nouveau mot de passe : </label>
<input type="password" id="pass" name="pass" required>
<br>
<input type="submit" value="Valider">
</form>
</c:if>
<c:if test="${! empty msg }">${msg} </c:if>
<br>
<form action="Connexion" method="post">
<input type="submit" value="retour">
</form>
</body>
</html>