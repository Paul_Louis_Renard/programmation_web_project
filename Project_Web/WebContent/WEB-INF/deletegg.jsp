<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Suppression d'un groupe dans un groupe</title>
</head>
<body>
<form action="deletegg" method="POST">
<p> Groupe hôte :  </p>
<select name="grp_on">
<c:forEach var="entry" items="${requestScope['Liste']}" >
	<option value="${entry.nom}">${entry.nom}</option>
</c:forEach>
</select>
<p> Groupe à supprimer du groupe hôte:  </p>
<select name="grp_in">
<c:forEach var="entry" items="${requestScope['Liste1']}" >
	<option value="${entry.nom}">${entry.nom}</option>
</c:forEach>
</select>
<br>
<br>
<p>Instruction: Choisissez votre groupe hôte puis validez. Ensuite reselectionnez le groupe hôte puis le groupe à supprimer. Appuyez ensuite sur valider pour supprimer effectivement le groupe</p>
<input type="submit" value="Valider">
</form>
<br>
<form action="options_Grp" method="POST">
<input type="submit" value="retour">
</form>
</body>
</html>