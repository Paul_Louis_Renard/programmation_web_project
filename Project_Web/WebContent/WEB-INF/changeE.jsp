<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Modifier un étudiant</title>
</head>
<body>
<c:if test="${!ok }">
<form action="changeStd" method="POST">
<select name="modif">
<c:forEach var="entry" items="${requestScope['etudiants']}" >
	<option value="${entry.numetu }">${entry.numetu}, ${entry.nom}, ${entry.prénom }</option>
</c:forEach>
</select>
<br>
<input type="submit" value="Valider">
</form>
</c:if>
<c:if test="${ok }">
<form action="changeStd" method="POST">

Ancien numéro étudiant : ${student.numetu } 
<br>
Nouveau numéro étudiant : <input type="number" name="numero" min="0">
<br>
Ancienne civilité : ${student.civilite }
<br>
<label for="civilite">Nouvelle civilité :</label>
  <select id="civilite" name="civilite">
    <option value="M">M</option>
    <option value="Mme">Mme</option>
    <option value="NB">NB</option>
  </select>
<br>
Ancien prénom : ${student.prénom }
<br>
Nouveau prénom : <input type="text" name="prenom">
<br>
Ancien nom : ${student.nom }
<br>
Nouveau nom : <input type="text" name="nom">
<br>
Ancienne date de naissance: ${student.datenaissance }
<br>
Nouvelle date de naissance : <input type="text" name="date de naissance">
<br>
Ancien courriel professionnel : ${student.mailpro }
<br>
Nouveau courriel professionnel : <input type="text" name="courriel pro">
<br>
<c:if test="${! empty student.mailperso }">Ancien courriel personnel: ${student.mailperso} <br> </c:if>
Nouvel courriel personnel : <input type="text" name="courriel perso" >
<br>
<c:if test="${! empty student.seriebac }">Ancienne série bac: ${student.seriebac} <br> </c:if>
Nouvelle série bac : <input type="text" name="serie bac" >
<br>
<c:if test="${! empty student.datebac }">Ancienne date bac: ${student.datebac} <br> </c:if>
Nouvelle date bac : <input type="text" name="date bac" >
<br>
<c:if test="${! empty student.mentionBac }">Ancienne mention bac: ${student.mentionBac} <br> </c:if>
Nouvelle mention bac : <input type="text" name="mention bac" >
<br>
<c:if test="${! empty student.diplome }">Ancienne diplome: ${student.diplome} <br> </c:if>
Nouveau diplome : <input type="text" name="diplome" >
<br>
<c:if test="${! empty student.datediplome }">Ancienne date diplome: ${student.datediplome} <br> </c:if>
Nouvelle date diplome : <input type="text" name="date diplome">
<br>
<c:if test="${! empty student.villediplome }">Ancienne ville diplome :  ${student.villediplome} <br> </c:if>
Nouvelle ville diplome : <input type="text" name="ville diplome">
<br>
<input type="submit" value="valider">
</form>
</c:if>
<form action="options_Std" method="POST">
<input type="submit" value="retour">
</form>
</body>
</html>