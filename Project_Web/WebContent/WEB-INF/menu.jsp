<%@ page contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Menu principal</title>
</head>
<body>
<form action="profile" method="post">
<input type="submit" value="Profil">
</form>
<br>
<c:choose>
	<c:when test="${ sessionScope.user.access == 0}">
	<form action="etudiantU" method="post">
		<input type="submit" value="Etudiant">
	</form>
	</c:when >
	<c:when test="${ sessionScope.user.access == 1}">

	<form action="etudiantA" method="post">
		<input type="submit" value="Etudiant">
	</form>
	</c:when>
	<c:when test="${ sessionScope.user.access == 2}">
	
	<form action="etudiantA" method="post">
		<input type="submit" value="Etudiant">
	</form>
	</c:when>
	
</c:choose>

<br>
<c:choose>
	<c:when test="${ sessionScope.user.access == 0}">
	<form action="groupeU" method="post">
		<input type="submit" value="Groupe">
	</form>
	</c:when>
	<c:when test="${ sessionScope.user.access == 1}">
	<form action="groupeA" method="post">
		<input type="submit" value="Groupe">
	</form>
	</c:when>
	<c:when test="${ sessionScope.user.access == 2}">
	<form action="groupeA" method="post">
		<input type="submit" value="Groupe">
	</form>
	</c:when>

</c:choose>
<br>
<form action="deconnexion" method="post">
<input type="submit" value="Déconnexion">
</form>
</body>
</html>