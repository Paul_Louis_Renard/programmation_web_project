<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Modifier le nom d'un groupe</title>
</head>
<body>
<c:if test="${! empty msg }">${msg}</c:if>
<form action="modifnom" method="POST">
<select name="groupe">
<c:forEach var="entry" items="${requestScope['groupes']}" >
	<option value="${entry.nom }">${entry.nom}</option>
</c:forEach>
</select>
<br>
Nouveau nom : 
<br>
<input type="text" name="name" required>
<input type="submit" value="Valider">
</form>
<form action="options_Grp" method="POST">
<input type="submit" value="retour">
</form>
</body>
</html>