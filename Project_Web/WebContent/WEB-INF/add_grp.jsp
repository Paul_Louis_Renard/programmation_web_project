<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ajout Groupe</title>
</head>
<body>
<form action="ajoutgroupe" method="POST">
nom du groupe:
<select name="group">
<c:forEach var="entry" items="${requestScope['groupes']}" >
	<option value="${entry.nom }">${entry.nom}</option>
</c:forEach>
</select>
<br>
<label for="table">Tableau des étudiants appartenant à ${nomchoisi } (Appuyer sur valider la première fois et vérifier que le nom du groupe est bien celui souhaité)</label>
<table id="table">
	<thead>
		<tr>
			<th>num_etudiant</th>
			<th>nom_etudiant</th>
			<th>prenom_etudiant</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="E" items="${requestScope['EtudiantGroupe']}" >
			<tr>
				<td>${E.numetu}</td>
            	<td>${E.nom }</td>
            	<td>${E.prénom}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<label for="listeEtu">Ajouter un étudiant:</label>
<input list="Liste" id="listeEtu" name="listeEtu" >
<datalist id="Liste">
    <c:forEach var="entry" items="${requestScope['Liste']}" >
		<option value="${entry.numetu}">${entry.numetu}, ${entry.nom}, ${entry.prénom }</option>
	</c:forEach>
</datalist>
<input type="submit" value="valider">
</form>
<br>
<form action="options_Grp" method="POST">
<input type="submit" value="retour">
</form>
</body>
</html>