<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Nouveau Groupe</title>
</head>
<body>
<form action="nouveaugroupe" method="POST">
nom du groupe:
<input type="text" id="nomGroupe" name="Nom" value="${nomgroupe}" required>
<br>
<table>
	<thead>
		<tr>
			<th>num_etudiant</th>
			<th>nom_etudiant</th>
			<th>prenom_etudiant</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="E" items="${requestScope['EtudiantGroupe']}" >
			<tr>
				<td>${E.numetu}</td>
            	<td>${E.nom }</td>
            	<td>${E.prénom}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<label for="listeEtu">choisissez des étudiants:</label>
<input list="Liste" id="listeEtu" name="listeEtu" >
<datalist id="Liste">
    <c:forEach var="entry" items="${requestScope['Liste']}" >
		<option value="${entry.numetu}">${entry.numetu}, ${entry.nom}, ${entry.prénom }</option>
	</c:forEach>
</datalist>


<input type="submit" value="ajouter">
</form>
<br>

<form action="options_Grp" method="POST">
<input type="submit" value="valider">
</form>
<form action="options_Grp" method="POST">
<input type="hidden" value="supprime" name="sup">
<input type="submit" value="retour">
</form>
</body>
</html>