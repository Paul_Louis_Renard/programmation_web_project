<%@ page pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Profil</title>
</head>
<body>
<c:if test="${!empty msg }">${msg }</c:if>
<br>
Bienvenue ${sessionScope.user.login}
<form action="deconnexion" method="post">
<input type="submit" value="Déconnexion">
</form>
<form action="menu" method="post">
<input type="submit" value="retour">
</form>
<br>
Vous êtes un ${role }
<br>
<form action="profile" method="post">
<label for="currentmdp">Votre mot de passe actuel : </label>
 <input type="password" name="currentmdp" id="currentmdp" required>
 <label for="newmdp">Votre nouveau mot de passe : </label>
<input type="password" name="newmdp" id="newmdp" required>
<input type="submit" value="Changer de mot de passe">
</form>
Votre question personnelle est : ${sessionScope.user.qperso}
<br>
Votre réponse à la question personnelle est : ${sessionScope.user.rperso} 
<form action="profile" method="post">
<label for="qperso" >Votre nouvelle question personnelle :</label>
 <input type="text" name="qperso" id="qperso" required>
<input type="submit" value="Changer la question personnelle">
</form>
<form action="profile" method="post">
 <label for="rperso" >Votre nouvelle réponse personnelle :</label>
 <input type="text" name="rperso" id="rperso" required>
<input type="submit" value="Changer la réponse à la question personnelle">
</form>
</body>
</html>