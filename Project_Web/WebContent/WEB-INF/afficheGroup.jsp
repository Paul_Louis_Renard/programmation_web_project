<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="refresh" content="25"/> <!-- Refresh toute les 25s -->
<title>Liste des groupes</title>
</head>
<body>
<c:choose>
	<c:when test="${ sessionScope.user.access == 0}">
	<form action="groupeU" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when >
	<c:when test="${ sessionScope.user.access == 1}">

	<form action="groupeA" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when>
	<c:when test="${ sessionScope.user.access == 2}">
	
	<form action="groupeA" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when>
	
</c:choose>
<c:forEach var="G" items="${requestScope['groupes']}" >
	<table id="table">
		<thead>
			<tr>
				<th>Nom</th>
				<th>Date de création</th>
				<th>Propriétaire</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>${G.nom}</td>
            	<td>${G.date_crea}</td>
            	<td>${G.proprio}</td>
			</tr>
		</tbody>
	<thead>
		<tr>
			<th>num_etudiant</th>
			<th>nom_etudiant</th>
			<th>prenom_etudiant</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="E" items="${G.grp}" >
			<tr>
				<td>${E.numetu}</td>
            	<td>${E.nom }</td>
            	<td>${E.prénom}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<br>
</c:forEach>

</body>
</html>