<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Etudiant ayant le même profil</title>
</head>
<body>
<script src="http://code.jquery.com/jquery-latest.js"></script> 
<button onclick="jQuery('#table').load(' #table');">Rafraichir</button>
<form action="profilEtudiant" method="POST">
<input type="hidden" value="1" name="id">
<input type="submit" value="Tri par Civilite">
</form>
<form action="profilEtudiant" method="POST">
<input type="hidden" value="2" name="id">
<input type="submit" value="Tri par Nom">
</form>
<form action="profilEtudiant" method="POST">
<input type="hidden" value="3" name="id">
<input type="submit" value="Tri par Prénom">
</form>
<form action="profilEtudiant" method="POST">
<input type="hidden" value="4" name="id">
<input type="submit" value="Tri par Date de naissance">
</form>
<form action="profilEtudiant" method="POST">
<input type="hidden" value="5" name="id">
<input type="submit" value="Tri par Série au baccalauréat">
</form>
<form action="profilEtudiant" method="POST">
<input type="hidden" value="6" name="id">
<input type="submit" value="Tri par date du baccalauréat">
</form>
<form action="profilEtudiant" method="POST">
<input type="hidden" value="7" name="id">
<input type="submit" value="Tri par Tri par Mention au baccalauréat">
</form>
<form action="profilEtudiant" method="POST">
<input type="hidden" value="8" name="id">
<input type="submit" value="Tri par Diplome">
</form>
<form action="profilEtudiant" method="POST">
<input type="hidden" value="9" name="id">
<input type="submit" value="Tri par Date d'obtention du diplome">
</form>
<form action="profilEtudiant" method="POST">
<input type="hidden" value="10" name="id">
<input type="submit" value="Tri par Ville du diplome">
</form>
<br>
<form action="listeStudent" method="POST">
<input type="submit" value="retour">
</form>
<div id=table>
   <table>
    <thead>
    <tr> <!-- ROW 1-->
      <th scope="col">Numéro étudiant</th>
      <th scope="col">Civilité</th>
      <th scope="col">Nom</th>
      <th scope="col">Prénom</th>
      <th scope="col">Date de Naissance</th>
      <th scope="col">Courriel professionnel</th>
      <th scope="col">Courriel personnel</th>
      <th scope="col">Série du baccalauréat</th>
      <th scope="col">Mention au baccalauréat</th>
      <th scope="col">Date d'obtention du baccalauréat</th>
      <th scope="col">Diplôme</th>
      <th scope="col">Date d'obtention du diplôme</th>
      <th scope="col">Ville d'obtention du diplôme</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="entry" items="${requestScope['etudiants']}" >
	<tr>
		<td>${entry.numetu }</td>
		<td>${entry.civilite }</td>
		<td>${entry.nom }</td>
		<td>${entry.prénom }</td>
		<td>${entry.datenaissance }</td>
		<td>${entry.mailpro }</td>
		<c:if test="${!empty entry.mailperso }">
		<td>${entry.mailperso }</td></c:if>
		<c:if test="${!empty entry.seriebac  }">
		<td>${entry.seriebac }</td> </c:if>
		<c:if test="${!empty entry.mentionBac }">
		<td>${entry.mentionBac }</td> </c:if>
		<c:if test="${!empty entry.datebac }">
		<td>${entry.datebac }</td> </c:if>
		<c:if test="${!empty entry.diplome }">
		<td>${entry.diplome }</td> </c:if>
		<c:if test="${!empty entry.datediplome }">
		<td>${entry.datediplome }</td> </c:if>
		<c:if test="${!empty entry.villediplome }">
		<td>${entry.villediplome }</td> </c:if>
	</tr>
</c:forEach>
  </tbody>
  </table>
</div>
</body>
</html>