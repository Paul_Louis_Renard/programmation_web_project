<%@ page pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta http-equiv="pragma" content="no-cache" /> 
<body>

<c:if test="${!empty err}" >${err} </c:if>

<form action="submission" method="post">
<label for="login">Login : </label>
<input type="text" name="login" id="login" required>
<br>
<label for="password">Password : </label>
<input type="password" name="password" id="password" required>
<br>
<input type="submit" value="Connexion">
</form>
<br>
<c:if test="${!empty err }">
<form action="oubli" method="post">
<input type="submit" value="Mot de passe oublié ?">
</form>
</c:if>
</body>
</html>
