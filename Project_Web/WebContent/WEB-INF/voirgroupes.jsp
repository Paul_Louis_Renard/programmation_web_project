<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>affichage des groupes pour un etudiant</title>
</head>
<body>
<form action="voirgroupes" method="post">
<label for="listeEtu">choisissez des étudiants:</label>
<input list="Liste" id="listeEtu" name="listeEtu" >
<datalist id="Liste">
    <c:forEach var="entry" items="${requestScope['Liste']}" >
		<option value="${entry.numetu}">${entry.numetu}, ${entry.nom}, ${entry.prénom }</option>
	</c:forEach>
</datalist>

<table>
	<thead>
		<tr>
			<th>nom_du_groupe</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="E" items="${requestScope['ListeGroupe']}" >
			<tr>
				<td>${E.nom}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>


<input type="submit" value="voir les groupes">
</form>
<br>


<c:choose>
	<c:when test="${ sessionScope.user.access == 0}">
	<form action="etudiantU" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when >
	<c:when test="${ sessionScope.user.access == 1}">

	<form action="etudiantA" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when>
	<c:when test="${ sessionScope.user.access == 2}">
	
	<form action="etudiantA" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when>
	
</c:choose>
</body>
</html>