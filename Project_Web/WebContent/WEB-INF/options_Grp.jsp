<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des options</title>
</head>
<body>
	<form action="nouveaugroupe" method="POST">
		<input type="submit" value="Créer un groupe">
	</form>
	<form action="ajoutgroupe" method="POST">
		<input type="submit" value="ajouter un étudiant dans un groupe">
	<br>
	</form>
	<form action="addgg" method="POST">
		<input type="submit" value="ajouter un groupe dans un groupe">
	<br>
	</form>
	<form action="deletegrp" method="POST">
		 <input type="submit" value="retirer un étudiant d'un groupe">
	<br>
	</form>
		<form action="deletegg" method="POST">
		 <input type="submit" value="retirer un groupe à un groupe">
	<br>
	</form>
	<form action="supprgrp" method="POST">
		<input type="submit" value="supprimer un groupe">
	</form>
	<form action="modifnom" method="POST">
		<input type="submit" value="modifier le nom d'un groupe">
	<br>
	</form>
	<form action="clonegrp" method="POST">
		<input type="submit" value="cloner un groupe">
	</form>
<c:choose>
	<c:when test="${ sessionScope.user.access == 0}">
	<form action="groupeU" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when >
	<c:when test="${ sessionScope.user.access == 1}">
	<form action="groupeA" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when>
	<c:when test="${ sessionScope.user.access == 2}">
	<form action="groupeA" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when>
	
</c:choose>
</body>
</html>