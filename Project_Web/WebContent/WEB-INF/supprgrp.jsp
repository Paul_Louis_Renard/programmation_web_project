<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Supprimer un groupe</title>
</head>
<body>
<form action="supprgrp" method="POST">
<p> Groupe : </p>
<select name="grp">
<c:forEach var="entry" items="${requestScope['Liste']}" >
	<option value="${entry.nom}">${entry.nom}</option>
</c:forEach>
</select>
<br>
<br>
<input type="submit" value="Valider">
</form>
<br>
<form action="options_Grp" method="POST">
<input type="submit" value="retour">
</form>
</body>
</html>