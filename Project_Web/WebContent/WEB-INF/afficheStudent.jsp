<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste Etudiants</title>
</head>
<body>
<script src="http://code.jquery.com/jquery-latest.js"></script> 
<button onclick="jQuery('#table').load(' #table');">Rafraichir</button>
<c:choose>
	<c:when test="${ sessionScope.user.access == 0}">
	<form action="etudiantU" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when >
	<c:when test="${ sessionScope.user.access == 1}">

	<form action="etudiantA" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when>
	<c:when test="${ sessionScope.user.access == 2}">
	
	<form action="etudiantA" method="post">
		<input type="submit" value="retour">
	</form>
	</c:when>
	
</c:choose>
<form action="profilEtudiant" method="POST">
<input type="submit" value="Voir tri étudiant par profil">
</form>
<div id=table>
  <table>
    <thead>
    <tr> <!-- ROW 1-->
      <th scope="col">Numéro étudiant</th>
      <th scope="col">Civilité</th>
      <th scope="col">Nom</th>
      <th scope="col">Prénom</th>
      <th scope="col">Date de Naissance</th>
      <th scope="col">Courriel professionnel</th>
      <th scope="col">Courriel personnel</th>
      <th scope="col">Série du baccalauréat</th>
       <th scope="col">Mention au baccalauréat</th>
      <th scope="col">Date d'obtention du baccalauréat</th>
      <th scope="col">Diplôme</th>
      <th scope="col">Date d'obtention du diplôme</th>
      <th scope="col">Ville d'obtention du diplôme</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="entry" items="${requestScope['etudiants']}" >
	<tr>
		<td>${entry.numetu }</td>
		<td>${entry.civilite }</td>
		<td>${entry.nom }</td>
		<td>${entry.prénom }</td>
		<td>${entry.datenaissance }</td>
		<td>${entry.mailpro }</td>
		<c:if test="${!empty entry.mailperso }">
		<td>${entry.mailperso }</td></c:if>
		<c:if test="${!empty entry.seriebac  }">
		<td>${entry.seriebac }</td> </c:if>
		<c:if test="${!empty entry.mentionBac }">
		<td>${entry.mentionBac }</td> </c:if>
		<c:if test="${!empty entry.datebac }">
		<td>${entry.datebac }</td> </c:if>
		<c:if test="${!empty entry.diplome }">
		<td>${entry.diplome }</td> </c:if>
		<c:if test="${!empty entry.datediplome }">
		<td>${entry.datediplome }</td> </c:if>
		<c:if test="${!empty entry.villediplome }">
		<td>${entry.villediplome }</td> </c:if>
	</tr>
</c:forEach>
  </tbody>
  </table>
</div>
  <c:if test="${sessionScope.user.access == 0}">
  <form method="POST" action="etudiantU" >
  <input type="submit" value="retour">
  </form>
  </c:if>
    <c:if test="${sessionScope.user.access == 1 || sessionScope.user.access==2}">

  </c:if>
</body>
</html>