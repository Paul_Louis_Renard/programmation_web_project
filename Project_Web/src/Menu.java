import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.DBManager;
import org.authentification.Utilisateur;

/**
 * Servlet affichant la page /Project_Web/menu
 * 
 * @author tonystark
 *
 */
public class Menu extends HttpServlet {
private void doProcess(HttpServletRequest request, HttpServletResponse response) {

		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null) {
			String pageName = "/WEB-INF/menu.jsp";
			RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
			try {
				rd.forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else { 
			try {
				String l="Connectez-vous d'abord";
				response.sendRedirect( "/Project_Web/Connexion" );
				request.setAttribute("err", l);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}
}
