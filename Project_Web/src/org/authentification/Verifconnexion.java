package org.authentification;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Verifconnexion {
 private String login;
 private String mdp;
 private Utilisateur u;
 private String msg;
 private String reponse;
 private HttpServletRequest request;
 
 public Verifconnexion(HttpServletRequest r) {
	 this.request=r;
 }
 public String getLogin() {
	return login;
}

public void setLogin(String login) {
	this.login = login;
}

public String getMdp() {
	return mdp;
}

public void setMdp(String mdp) {
	this.mdp = mdp;
}

public Utilisateur getU() {
	return u;
}

public void setU(Utilisateur u) {
	this.u = u;
}

public String getReponse() {
	return reponse;
}
public void setReponse(String reponse) {
	this.reponse = reponse;
}
public String getMsg() {
	return msg;
}

public void setMsg(String msg) {
	this.msg = msg;
}

public HttpServletRequest getRequest() {
	return request;
}

public void setRequest(HttpServletRequest request) {
	this.request = request;
}


 
public Verifconnexion(String l,String m,HttpServletRequest request) {
	this.login=l;
	this.mdp=m;
	this.request=request;
}

public boolean is_correct() {
	Connection c=DBManager.getInstance().getConnection();
	
	try {
		Statement s=c.createStatement();
		ResultSet rs=s.executeQuery("select * from Users where login='"+login+"' and pass='"+mdp+"'");
		if(!rs.next()) {
			msg="Le mot de passe ou le nom d'utilisateur que vous avez rentré est incorrect";
			request.setAttribute("err", msg);
			return false;
		}
		else {
			String i=rs.getString(1);
			int id=Integer.parseInt(i);
			String nom=rs.getString(2);
			String password=rs.getString(3);
			String question=rs.getString(4);
			String reponse=rs.getString(5);
			int access=Integer.parseInt(rs.getString(6));
			Utilisateur u=new Utilisateur(id,nom, password, question, reponse, access);
			this.msg="Connexion réussi avec succès. Redirection...";
			HttpSession session=this.request.getSession();
			session.setAttribute("user", u);
			request.setAttribute("succ", msg);
			c.close();
			return true;
		}
		
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return false;

}





}
