package org.authentification;

public class Utilisateur {
	private int id;
	private String login;
	private String pass;
	private String qperso;
	private String rperso;
	private int access;
	
	public Utilisateur(int id,String l,String p,String q,String r,int a) {
		this.setId(id);
		this.login=l;
		this.pass=p;
		this.qperso=q;
		this.rperso=r;
		this.access=a;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getQperso() {
		return qperso;
	}
	public void setQperso(String qperso) {
		this.qperso = qperso;
	}
	public String getRperso() {
		return rperso;
	}
	public void setRperso(String rperso) {
		this.rperso = rperso;
	}
	public int getAccess() {
		return access;
	}
	public void setAccess(int access) {
		this.access = access;
	}
	public String toStringAcces() {
		switch(access) {
		case 0: return "Utilisateur Standard"; 
		case 1: return "Editeur"; 
		case 2: return "Administrateur";
		}
		return "";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

}
