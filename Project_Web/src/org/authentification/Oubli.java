package org.authentification;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.profile.ModifProfil;

/**
 * Servlet affichant la page /Project_Web/oubli
 * 
 * @author tonystark
 *
 */
public class Oubli extends HttpServlet  {
private void doProcess(HttpServletRequest request, HttpServletResponse response) {

		
		String pageName = "/WEB-INF/oublimdp.jsp";
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		OubliMet v=new OubliMet(request);
		if(request.getParameter("pseudo")!=null) {
			if(v.question_secrete()) {
				request.setAttribute("ok1", true);
			}
		}
		
		if(request.getParameter("rperso")!=null) {
			System.out.println(request.getParameter("rperso"));
			System.out.println("login="+v.getLogin());
			
			if(request.getParameter("rperso").equals(v.getReponse())) {
				request.setAttribute("ok2", true);
			}
		}
		if(request.getParameter("pass")!=null) {
			ModifProfil modifp=new ModifProfil(request);
			modifp.modifmdp(v.getLogin());
		}
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}
}
