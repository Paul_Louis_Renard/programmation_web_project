package org.authentification;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;

public class OubliMet {
	private static String login;
	private static String reponse;
	private HttpServletRequest request;
	
	public OubliMet(HttpServletRequest r) {
		this.request=r;
	}

	public boolean question_secrete() {
		Connection c=DBManager.getInstance().getConnection();
		try {
			this.login=request.getParameter("pseudo");
			Statement s=c.createStatement();
			ResultSet rs=s.executeQuery("select * from Users where login='"+login+"'");
			if(!rs.next()) {
				String msg="Le nom d'utilisateur que vous avez rentré n'existe pas";
				request.setAttribute("err", msg);
				return false;
			}
			else {
				String question=rs.getString(4);
				String reponse=rs.getString(5);
				this.reponse=reponse;
				request.setAttribute("question", question);
				return true;
			}
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return false;

	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String lo) {
		this.login=lo;
	}
	
	
	public String getReponse() {
		return reponse;
	}
}
