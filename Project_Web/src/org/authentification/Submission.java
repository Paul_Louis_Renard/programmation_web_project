package org.authentification;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet affichant la page /Project_Web/submission
 * 
 * @author tonystark
 *
 */
public class Submission extends HttpServlet{
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		String login = request.getParameter("login");
		String mdp=request.getParameter("password");
		String pageName = new String();
		Verifconnexion v=new Verifconnexion(login,mdp,request);
		
		if(v.is_correct()) {
			pageName="/WEB-INF/submission.jsp";
		}
		else {
			pageName="/WEB-INF/connexion.jsp";
		}
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}
	

}
