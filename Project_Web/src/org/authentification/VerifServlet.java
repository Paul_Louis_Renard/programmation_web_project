package org.authentification;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet affichant la page /Project_Web/submission
 * 
 * @author tonystark
 *
 */
public class VerifServlet extends HttpServlet {

	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		String msg=new String();
		String pageName =new String();
		String cpassword=request.getParameter("currentmdp");
		String npassword=request.getParameter("newmdp");
		String newquestion=request.getParameter("qperso");
		String newresponse=request.getParameter("rperso");
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null) { //S'il y a une session utilisateur ouverte
			if(cpassword!=null && npassword!=null) { //On renvoie les donnée à profile pour qu'il les traite
				request.setAttribute("currentmdp", cpassword);
				request.setAttribute("newmdp", npassword);
			}
			if(newquestion!=null) {
				request.setAttribute("qperso", newquestion);
				
			}
			if(newresponse!=null) {
				request.setAttribute("rperso",newresponse);
			}
			pageName="/WEB-INF/profile.jsp";
			RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
			try {
				rd.forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else { //Sinon, on se redirige directement vers la page de connexion en donnant des infos
			try {
				String l="Connectez-vous pour modifier vos paramètres";
				request.setAttribute("err", l);
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}
}
