package org.profile;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.authentification.DBManager;
import org.authentification.Utilisateur;

public class ModifProfil {

	 private HttpServletRequest request;
	 
	 public ModifProfil(HttpServletRequest r) {
		 this.request=r;
	 }
	 
	 public HttpServletRequest getRequest() {
		return request;
	}



	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}



	public boolean modif() {
			Connection c=DBManager.getInstance().getConnection();
			
			try {
				Statement s=c.createStatement();
				String msg=new String();
				Utilisateur u=(Utilisateur) request.getSession().getAttribute("user");
				if(u!=null) {
					String poste=u.toStringAcces();
					request.setAttribute("role", poste);
					String cpassword=request.getParameter("currentmdp");
					String npassword=request.getParameter("newmdp");
					String newquestion=request.getParameter("qperso");
					String newresponse=request.getParameter("rperso");
					if(cpassword!=null && npassword!=null) {
						ResultSet rs=s.executeQuery("select * from Users where login='"+u.getLogin()+"' and pass='"+cpassword+"'");
						if(!rs.next()) {
							msg="Le mot de passe que vous avez rentré n'est pas correct";
							request.setAttribute("msg", msg);
						}
						else {
							/* Exécution d'une requête d'écriture */
							int statut = s.executeUpdate( "UPDATE Users Set pass='"+npassword+"' WHERE id="+u.getId());
							u.setPass(npassword);
							msg="Le mot de passe a été modifié";
							request.setAttribute("msg", msg);
						}
					}
					if(newquestion!=null) {
						int statut=s.executeUpdate("UPDATE Users set persoQ='"+newquestion+"' WHERE id="+u.getId());
						
						u.setQperso(newquestion);
						msg="La question personnelle a été modifiée";
						request.setAttribute("msg", msg);
					}
					if(newresponse!=null) {
						int statut=s.executeUpdate("UPDATE Users set persoR='"+newresponse+"' WHERE id="+u.getId());
						u.setRperso(newresponse);
						msg="La réponse à la question personnelle a été modifiée";
						request.setAttribute("msg", msg);
					}
					c.close();
					return true;
				}
				else {
					c.close();
					return false;
				}
				
				
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
	 }
	public boolean modifmdp(String id) { //utiliser dans l'oubli de mdp
		Connection c=DBManager.getInstance().getConnection();
		Statement s = null;
		try {
			s = c.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String msg=new String();
		String npassword=request.getParameter("pass");
		int statut=0;
	/* Exécution d'une requête d'écriture */
		try {
			statut = s.executeUpdate( "UPDATE Users Set pass='"+npassword+"' WHERE login='"+id+"'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		msg="Le mot de passe a été modifié";
		request.setAttribute("msg", msg);
		if(statut>0) {
			return true;
		}
		else{
			return false;
		}
		}
	
}
