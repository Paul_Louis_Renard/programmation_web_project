package org.get_etudiant;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.authentification.DBManager;
import org.authentification.Utilisateur;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

public class JSON_reader {
	
    public static String getText(String url) throws IOException {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader( new InputStreamReader(connection.getInputStream(),"UTF8"));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null) 
            response.append(inputLine);
        
        in.close();
        return response.toString();
    }
    
	public static void main(String[] args) throws IOException, JSONException {
		String jsonStr = "https://stormy-lowlands-39083.herokuapp.com/etudiants";
		JSONArray jsonarray = new JSONArray(getText(jsonStr));
		
		Connection co=DBManager.getInstance().getConnection();
		
		for (int i = 0; i < jsonarray.length(); i++) { // pour chaque json
		    JSONObject jsonobject = jsonarray.getJSONObject(i);

		    String num = jsonobject.getString("numetudiant");
			String c =jsonobject.getString("S");
			String prenom = jsonobject.getString("prenom");
			String nom = jsonobject.getString("nom");
			String dnaissance = jsonobject.getString("ddn");
			String cpro= jsonobject.getString("emailPro");
			String cperso = jsonobject.getString("emailPerso");
			String sbac = jsonobject.getString("bac");
			String dbac = jsonobject.getString("anBac");
			String mbac= jsonobject.getString("menBac");
			String diplome = jsonobject.getString("diplome");
			String ddiplome = jsonobject.getString("anDiplome");
			String vdiplome = jsonobject.getString("villeDiplome");
			
			try {
				Statement s=co.createStatement();
				int statut = s.executeUpdate("INSERT INTO Etudiants (numetudiant, civilite, nom, prenom, dateNaissance, courrielPro, courrielPerso, serieBac, dateBac, mentionBac, diplome, dateDiplome, villeDiplome, proprio) VALUES ('"+num+"','"+c+"','"+nom+"','"+prenom+"','"+dnaissance+"','"+cpro+"','"+cperso+"','"+sbac+"','"+dbac+"','"+mbac+"','"+diplome+"','"+ddiplome+"','"+vdiplome+"','superRoot')");

			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    System.out.println(jsonobject.toString());
		}
  }
}
