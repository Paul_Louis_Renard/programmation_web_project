package org.etudiant;

import java.util.Comparator;

public class SortbyMentionBac implements Comparator<Etudiant> 
{ 
	@Override
	public int compare(Etudiant o1, Etudiant o2) {
		return o1.getMentionBac().compareTo(o2.getMentionBac());
	} 
} 
