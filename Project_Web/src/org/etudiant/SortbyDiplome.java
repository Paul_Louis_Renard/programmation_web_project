package org.etudiant;

import java.util.Comparator;

public class SortbyDiplome implements Comparator<Etudiant> 
{ 
	@Override
	public int compare(Etudiant o1, Etudiant o2) {
		return o1.getDiplome().compareTo(o2.getDiplome());
	} 
} 
