package org.etudiant;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.Utilisateur;

public class ProfilEtudiantServlet extends  HttpServlet{
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		String pageName = "/WEB-INF/profileEtudiant.jsp";
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
			Liste_Etu_Modif lili=new Liste_Etu_Modif(request);
			if(request.getParameter("id")!=null) {
				int id=Integer.parseInt(request.getParameter("id"));
				LinkedList<Etudiant> et=lili.tri(id);
				request.setAttribute("etudiants", et);
			}
			try {
				rd.forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
			
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			doProcess(req, resp);
		}
}
