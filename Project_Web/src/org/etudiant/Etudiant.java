package org.etudiant;

import java.util.Comparator;

public class Etudiant {
private String numetu;
private String civilite;
private String prénom;
private String nom;
private String datenaissance;
private String mailpro;
private String mailperso;
private String seriebac;
private String mentionBac;
private String datebac;
private String diplome;
private String datediplome;
private String villediplome;
public String getNumetu() {
	return numetu;
}
public void setNumetu(String numetu) {
	this.numetu = numetu;
}
public String getCivilite() {
	return civilite;
}
public void setCivilite(String civilite) {
	this.civilite = civilite;
}
public String getPrénom() {
	return prénom;
}
public void setPrénom(String prénom) {
	this.prénom = prénom;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getDatenaissance() {
	return datenaissance;
}
public void setDatenaissance(String datenaissance) {
	this.datenaissance = datenaissance;
}
public String getMailpro() {
	return mailpro;
}
public void setMailpro(String mailpro) {
	this.mailpro = mailpro;
}
public String getMailperso() {
	return mailperso;
}
public void setMailperso(String mailperso) {
	this.mailperso = mailperso;
}
public String getSeriebac() {
	return seriebac;
}
public void setSeriebac(String seriebac) {
	this.seriebac = seriebac;
}
public String getDatebac() {
	return datebac;
}
public void setDatebac(String datebac) {
	this.datebac = datebac;
}
public String getDiplome() {
	return diplome;
}
public void setDiplome(String diplome) {
	this.diplome = diplome;
}
public String getDatediplome() {
	return datediplome;
}
public void setDatediplome(String datediplome) {
	this.datediplome = datediplome;
}
public String getVillediplome() {
	return villediplome;
}
public void setVillediplome(String villediplome) {
	this.villediplome = villediplome;
}
public String getMentionBac() {
	return mentionBac;
}
public void setMentionBac(String mentionBac) {
	this.mentionBac = mentionBac;
}
}
