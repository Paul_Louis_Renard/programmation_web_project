package org.etudiant;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.DBManager;
import org.authentification.Utilisateur;
import org.etudiant.Etudiant;
import org.etudiant.Liste_Etu_Modif;
import org.groupe.AjoutGp;
import org.groupe.Groupe;
import org.groupe.Liste_Groupe_Modif;


public class VoirGroupes extends HttpServlet {
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null) {
		String pageName = "/WEB-INF/voirgroupes.jsp";
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		Liste_Groupe_Modif l=new Liste_Groupe_Modif(request);
		Liste_Etu_Modif l2 = new Liste_Etu_Modif(request);
		LinkedList<Etudiant> liste=l.list();
		String numetu = request.getParameter("listeEtu");
		l2.setNumEtu(numetu);
		System.out.println(numetu);
		LinkedList<Groupe> listeGroupe = l2.ListeGroupe();
		request.setAttribute("Liste", liste);
		request.setAttribute("ListeGroupe", listeGroupe);
		
		
		
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}
}
