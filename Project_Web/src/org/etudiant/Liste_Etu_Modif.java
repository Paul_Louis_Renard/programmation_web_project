package org.etudiant;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;

import org.authentification.DBManager;
import org.authentification.Utilisateur;
import org.groupe.Groupe;

/*Cette classe retourne les étudiants que peut modifier la personne */
public class Liste_Etu_Modif {
private HttpServletRequest request;
private static String idupdate; //Permet de conserver id malgré le rechargement de la page
private static String nomgrp;	//Permet de conserver le nom du groupe malgré le rechargement de la page
private static String numetu;

public Liste_Etu_Modif(HttpServletRequest r) {
	this.request=r;
}

public String getNomGrp() {
	return nomgrp;
}
public void setNomGrp(String nom) {
	nomgrp=nom;
}

public String getNumEtu() {
	return numetu;
}
public void setNumEtu(String num) {
	numetu=num;
}

public LinkedList<Etudiant> list(){ //Permet d'avoir la liste des étudiants modifiable pour un éditeur ou un admin
	LinkedList<Etudiant> e=new LinkedList<Etudiant>();
	Utilisateur u=(Utilisateur) request.getSession().getAttribute("user");
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		ResultSet rs;
		if(u.getAccess()==1) {
			rs=s.executeQuery("select * from Etudiants where proprio='"+u.getLogin()+"'");
		}
		else {
			rs=s.executeQuery("select * from Etudiants");
		}
		while(rs.next()) {
			Etudiant et=new Etudiant();
			et.setNumetu(rs.getString(1));
			et.setCivilite(rs.getString(2));;
			et.setNom(rs.getString(3));
			et.setPrénom(rs.getString(4));
			et.setDatenaissance(rs.getString(5));
			et.setMailpro(rs.getString(6));
			et.setMailperso(rs.getString(7));
			et.setSeriebac(rs.getString(8));
			et.setDatebac(rs.getString(9));
			et.setMentionBac(rs.getString(10));
			et.setDiplome(rs.getString(11));
			et.setDatediplome(rs.getString(12));
			et.setVillediplome(rs.getString(13));
			e.add(et);
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return e;
}

public LinkedList<Etudiant> listcomplet(){ 
	LinkedList<Etudiant> e=new LinkedList<Etudiant>();
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		ResultSet rs;
		rs=s.executeQuery("select * from Etudiants");
		
		while(rs.next()) {
			Etudiant et=new Etudiant();
			et.setNumetu(rs.getString(1));
			et.setCivilite(rs.getString(2));
			et.setNom(rs.getString(3));
			et.setPrénom(rs.getString(4));
			et.setDatenaissance(rs.getString(5));
			et.setMailpro(rs.getString(6));
			et.setMailperso(rs.getString(7));
			et.setSeriebac(rs.getString(8));
			et.setDatebac(rs.getString(9));
			et.setMentionBac(rs.getString(10));
			et.setDiplome(rs.getString(11));
			et.setDatediplome(rs.getString(12));
			et.setVillediplome(rs.getString(13));
			e.add(et);
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return e;
}

public Etudiant recup(String id) {
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		ResultSet rs;
		rs=s.executeQuery("select * from Etudiants where numetudiant='"+id+"'");
		rs.next();
		Etudiant et=new Etudiant();
		et.setNumetu(rs.getString(1));
		et.setCivilite(rs.getString(2));
		et.setNom(rs.getString(3));
		et.setPrénom(rs.getString(4));
		et.setDatenaissance(rs.getString(5));
		et.setMailpro(rs.getString(6));
		et.setMailperso(rs.getString(7));
		et.setSeriebac(rs.getString(8));
		et.setDatebac(rs.getString(9));
		et.setMentionBac(rs.getString(10));
		et.setDiplome(rs.getString(11));
		et.setDatediplome(rs.getString(12));
		et.setVillediplome(rs.getString(13));
		return et;

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return null;
}

public boolean update(String id) {
	String num = request.getParameter("numero");
	String c=request.getParameter("civilite");
	String prenom = request.getParameter("prenom");
	String nom=request.getParameter("nom");
	String dnaissance = request.getParameter("date de naissance");
	String cpro=request.getParameter("courriel pro");
	String cperso = request.getParameter("courriel perso");
	String sbac=request.getParameter("serie bac");
	String dbac = request.getParameter("date bac");
	String mbac=request.getParameter("mention bac");
	String diplome = request.getParameter("diplome");
	String ddiplome=request.getParameter("date diplome");
	String vdiplome = request.getParameter("ville diplome");
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		if(c!=null && !c.equals("")) {
			s.executeUpdate("UPDATE Etudiants set civilite='"+c+"' WHERE numetudiant='"+id+"'");
		}
		if(prenom!=null && !prenom.equals("")) {
			s.executeUpdate("UPDATE Etudiants set prenom='"+prenom+"' WHERE numetudiant='"+id+"'");
		}
		if(nom!=null && !nom.equals("")) {
			s.executeUpdate("UPDATE Etudiants set nom='"+nom+"' WHERE numetudiant='"+id+"'");
		}
		if(dnaissance!=null && !dnaissance.equals("")) {
			s.executeUpdate("UPDATE Etudiants set dateNaissance='"+dnaissance+"' WHERE numetudiant='"+id+"'");
		}
		if(cpro!=null && !cpro.equals("")) {
			s.executeUpdate("UPDATE Etudiants set courrielPro ='"+cpro+"' WHERE numetudiant='"+id+"'");
		}
		if(cperso!=null && !cperso.equals("")) {
			s.executeUpdate("UPDATE Etudiants set courrielPerso  ='"+cperso+"' WHERE numetudiant='"+id+"'");
		}
		if(cperso!=null && !cperso.equals("")) {
			s.executeUpdate("UPDATE Etudiants set courrielPerso  ='"+cperso+"' WHERE numetudiant='"+id+"'");
		}	
		if(sbac!=null && !sbac.equals("")) {
			s.executeUpdate("UPDATE Etudiants set serieBac  ='"+sbac+"' WHERE numetudiant='"+id+"'");
		}
		if(dbac!=null && !dbac.equals("")) {
			s.executeUpdate("UPDATE Etudiants set dateBac  ='"+dbac+"' WHERE numetudiant='"+id+"'");
		}
		if(mbac!=null && !mbac.equals("")) {
			s.executeUpdate("UPDATE Etudiants set mentionBac  ='"+mbac+"' WHERE numetudiant='"+id+"'");
		}
		if(diplome!=null && !diplome.equals("")) {
			s.executeUpdate("UPDATE Etudiants set diplome ='"+diplome+"' WHERE numetudiant='"+id+"'");
		}
		if(ddiplome!=null && !ddiplome.equals("")) {
			s.executeUpdate("UPDATE Etudiants set dateDiplome ='"+ddiplome+"' WHERE numetudiant='"+id+"'");
		}
		if(vdiplome!=null && !vdiplome.equals("")) {
			s.executeUpdate("UPDATE Etudiants set villeDiplome ='"+vdiplome+"' WHERE numetudiant='"+id+"'");
		}
		if(num!=null && !num.equals("")) {
			ResultSet rs=s.executeQuery("select * from Etudiants where numetudiant='"+num+"'");
			if(!rs.next()) { //Cas où on va écraser un identifiant existant
				s.executeUpdate("UPDATE Etudiants set numetudiant='"+num+"' WHERE numetudiant='"+id+"'");
				s.executeUpdate("update EG set numetudiant='"+num+"' where numetudiant='"+id+"'");
			}
			else {
				return false;
			}
		}
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return true;
}

public boolean suppression(String id) {
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		int statut = s.executeUpdate("DELETE from Etudiants WHERE numetudiant='"+id+"'");
		int statut2 = s.executeUpdate("delete from EG where numetudiant='"+id+"'");
		return true;
		

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return false;
}

public LinkedList<Etudiant> etuGroupe() {
	Connection co=DBManager.getInstance().getConnection();
	LinkedList<Etudiant> e=new LinkedList<Etudiant>();
	try {
		Statement s=co.createStatement();
		ResultSet rs;
		rs=s.executeQuery("select EG.numetudiant,nom,prenom from EG,Etudiants where nom_grp='"+this.getNomGrp()+"' and EG.numetudiant=Etudiants.numetudiant");
		
		while(rs.next()) {
			Etudiant et=new Etudiant();
			et.setNumetu(rs.getString(1));
			et.setNom(rs.getString(2));
			et.setPrénom(rs.getString(3));
			e.add(et);
		}
	}
	catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return e;
	}

public LinkedList<Etudiant> etuNonGroupe() {
	Connection co=DBManager.getInstance().getConnection();
	LinkedList<Etudiant> e=new LinkedList<Etudiant>();
	try {
		Statement s=co.createStatement();
		ResultSet rs;
		rs=s.executeQuery("Select numetudiant,nom,prenom from Etudiants where numetudiant NOT in (Select numetudiant from EG where nom_grp='"+this.getNomGrp()+"')");
		
		while(rs.next()) {
			Etudiant et=new Etudiant();
			et.setNumetu(rs.getString(1));
			et.setNom(rs.getString(2));
			et.setPrénom(rs.getString(3));
			e.add(et);
		}
	}
	catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return e;
	}

public LinkedList<Etudiant> tri(int id){
	LinkedList<Etudiant> l=this.listcomplet();
	switch(id) {
	case 1: Collections.sort(l, new SortbyCivilite()); break; //civilite
	case 2: Collections.sort(l, new SortbyNom());break; //nom
	case 3: Collections.sort(l, new SortbyPrenom());break; //prenom
	case 4: Collections.sort(l, new SortbyDateNaissance());break; //dateNaissance
	case 5: Collections.sort(l, new SortbySerieBac());break; //serieBac
	case 6: Collections.sort(l, new SortbyDateBac());break; //dateBac
	case 7: Collections.sort(l, new SortbyMentionBac());break; //mentionBac
	case 8: Collections.sort(l, new SortbyDiplome());break; //diplome
	case 9: Collections.sort(l, new SortbyDateDiplome());break; //dateDiplome
	case 10: Collections.sort(l, new SortbyVilleDiplome());break; //villeDiplome
	}
	return l;
}

public static String getIdupdate() {
	return idupdate;
}

public static void setIdupdate(String idupdate) {
	Liste_Etu_Modif.idupdate = idupdate;
}

public LinkedList<Groupe> ListeGroupe() {
	Connection co=DBManager.getInstance().getConnection();
	LinkedList<Groupe> g=new LinkedList<Groupe>();
	try {
		Statement s=co.createStatement();
		ResultSet rs;
		if(this.getNumEtu() != null) {
			rs=s.executeQuery("select distinct nom_grp from EG where numetudiant='"+this.getNumEtu()+"'");
			while(rs.next()) {
				Groupe gp=new Groupe();
				gp.setNom(rs.getString(1));
				g.add(gp);
			}
		}
		
		
		
	}
	catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return g;
	}

}
