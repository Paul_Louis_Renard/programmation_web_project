package org.etudiant;

import java.util.Comparator;

public class SortbySerieBac implements Comparator<Etudiant> 
{ 
	@Override
	public int compare(Etudiant o1, Etudiant o2) {
		return o1.getSeriebac().compareTo(o2.getSeriebac());
	} 
} 
