package org.etudiant;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.Utilisateur;

public class Add_std extends  HttpServlet{
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		String pageName = "/WEB-INF/add_std.jsp";
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null && u.getAccess()>=1) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
			String msg=new String();
			System.out.println(request.getParameter("numero"));
			if(request.getParameter("numero")!=null) {
				AjoutEt ajoutet=new AjoutEt(request);
				boolean v=ajoutet.ajout();
				if(v) {
					msg="Ajout réalisé avec succès!";
				}
				else {
					msg="L'étudiant existe déjà !";
				}
				request.setAttribute("msg", msg);
			}
			
			
			
			
			try {
				rd.forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

			
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			doProcess(req, resp);
		}
}


