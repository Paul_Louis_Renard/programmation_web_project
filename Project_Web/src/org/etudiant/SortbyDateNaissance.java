package org.etudiant;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class SortbyDateNaissance implements Comparator<Etudiant> 
{ 
	@Override
	public int compare(Etudiant o1, Etudiant o2) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		Date d=null;
		Date d1=null;
		try {
			d = sdf.parse(o1.getDatenaissance());
			d1=sdf.parse(o2.getDatenaissance());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return d.compareTo(d1);
	} 
} 
