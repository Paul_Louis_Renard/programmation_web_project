package org.etudiant;

import java.util.Comparator;

public class SortbyPrenom implements Comparator<Etudiant> 
{ 
	@Override
	public int compare(Etudiant o1, Etudiant o2) {
		return o1.getPrénom().compareTo(o2.getPrénom()); 
	} 
} 
