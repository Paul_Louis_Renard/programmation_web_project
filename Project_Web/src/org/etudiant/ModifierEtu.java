package org.etudiant;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.Utilisateur;

public class ModifierEtu extends  HttpServlet {
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		String pageName = "/WEB-INF/changeE.jsp";
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null && u.getAccess()>=1) {
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		if(request.getParameter("modif")==null) {
			Liste_Etu_Modif l=new Liste_Etu_Modif(request);
			if(l.getIdupdate()!=null) {
				l.update(l.getIdupdate());
			}
			LinkedList<Etudiant> etudiants=l.list();
			request.setAttribute("etudiants", etudiants);
			
		}
		else {
			request.setAttribute("ok", true);
			String id=request.getParameter("modif");
			Liste_Etu_Modif l=new Liste_Etu_Modif(request);
			Etudiant bob=l.recup(id);
			request.setAttribute("student", bob);
			l.setIdupdate(request.getParameter("modif"));
			
		}
		Liste_Etu_Modif l=new Liste_Etu_Modif(request);
		if(l.getIdupdate()!=null) {
			l.update(l.getIdupdate());
		}
		
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			doProcess(req, resp);
		}
}
