package org.etudiant;

import java.util.Comparator;

public class SortbyCivilite implements Comparator<Etudiant> 
{ 
	@Override
	public int compare(Etudiant o1, Etudiant o2) {
		return o1.getCivilite().compareTo(o2.getCivilite()); 
	} 
} 