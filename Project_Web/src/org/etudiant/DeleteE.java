package org.etudiant;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.Utilisateur;

public class DeleteE extends  HttpServlet{
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		String pageName = "/WEB-INF/deleteE.jsp";
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null && u.getAccess()>=1) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
			Liste_Etu_Modif list=new Liste_Etu_Modif(request);
			String asupprimer=request.getParameter("sup");
			if(asupprimer!=null) {
				boolean verif=list.suppression(asupprimer);
				if(verif) {
					request.setAttribute("msg", "L'étudiant a été supprimé");
				}
				else {
					request.setAttribute("msg", "Une erreur s'est produite");
				}
			}
			
			LinkedList<Etudiant> ed=list.list();
			if(!ed.isEmpty()) {
				request.setAttribute("etudiants", ed);
			}
			
			
			try {
				rd.forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

			
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			doProcess(req, resp);
		}
}
