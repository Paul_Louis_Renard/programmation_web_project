package org.groupe;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;

import org.authentification.DBManager;
import org.authentification.Utilisateur;
import org.etudiant.Etudiant;
import org.etudiant.Liste_Etu_Modif;

/*Cette classe réalise toutes les modifications que l'on veut faire sur un groupe */
public class Liste_Groupe_Modif {
private HttpServletRequest request;
private static String group;

public Liste_Groupe_Modif(HttpServletRequest r) {
	this.request=r;
}

public LinkedList<Etudiant> list(){
	LinkedList<Etudiant> e=new LinkedList<Etudiant>();
	Utilisateur u=(Utilisateur) request.getSession().getAttribute("user");
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		ResultSet rs=null;
		String nomgroupe=request.getParameter("Nom");
		if(nomgroupe!=null) {
			rs=s.executeQuery("select numetudiant, nom, prenom from Etudiants where numetudiant NOT IN (SELECT numetudiant from EG where nom_grp='"+nomgroupe+"' )");
		}
		else {
			rs=s.executeQuery("select numetudiant, nom, prenom from Etudiants");
		}
		while(rs.next()) {
			Etudiant et=new Etudiant();
			et.setNumetu(rs.getString(1));
			et.setNom(rs.getString(2));
			et.setPrénom(rs.getString(3));
			e.add(et);
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return e;
}

public LinkedList<Groupe> listgg(){
	LinkedList<Groupe> g=new LinkedList<Groupe>();
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		ResultSet rs;
		rs=s.executeQuery("select * from Groupes");
		while(rs.next()) {
			Groupe grp=new Groupe();
			grp.setNom(rs.getString(1));
			g.add(grp);
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return g;
}

public LinkedList<Groupe> listselondroit(){
	LinkedList<Groupe> g=new LinkedList<Groupe>();
	Utilisateur u=(Utilisateur) request.getSession().getAttribute("user");
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		ResultSet rs;
		if(u.getAccess()==2) {
			rs=s.executeQuery("select * from Groupes");
		}
		else {
			rs=s.executeQuery("select * from Groupes where proprio='"+u.getLogin()+"'");
		}
		
		while(rs.next()) {
			Groupe grp=new Groupe();
			grp.setNom(rs.getString(1));
			g.add(grp);
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return g;
}

public boolean suppression(String id, String nom) {
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		int statut = s.executeUpdate("DELETE from EG WHERE numetudiant='"+id+"' AND nom_grp='"+nom+"'");
		if(statut==1) {
			return true;
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return false;
}

public boolean ajoutgg(String grp_on,String grp_in) {
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		int statut = s.executeUpdate("INSERT INTO GG(grp_on,grp_in) VALUES ('"+grp_on+"','"+grp_in+"')");
		int statut2 = s.executeUpdate("INSERT INTO EG(numetudiant, nom_grp)\n" + 
				"SELECT numetudiant,'"+grp_on+"' \n" + 
				"FROM EG \n" + 
				"WHERE nom_grp = '"+grp_in+"'");
		if((statut==1)&&(statut==1)) {
			return true;
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return false;
}

public boolean supprgrp(String grp) {
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		int statut = s.executeUpdate("delete from Groupes where nom='"+grp+"'");
		int statut2 = s.executeUpdate("delete from EG where nom_grp='"+grp+"'");
		int statut3 = s.executeUpdate("delete from GG where grp_in='"+grp+"' or grp_on='"+grp+"'");
		if ((statut==1)&&(statut2==1)&&(statut3==1)) {
			return true;
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return false;
}

public boolean supprgg(String grp_on,String grp_in) {
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		int statut = s.executeUpdate("delete from GG where grp_on='"+grp_on+"' and grp_in='"+grp_in+"'");
		s.executeUpdate("create table temp(num varchar(100) primary key)");
		s.executeUpdate("insert into temp(num) select numetudiant from EG where nom_grp='"+grp_in+"'");
		int statut2 = s.executeUpdate("delete from EG where nom_grp='"+grp_on+"' and numetudiant in (select num from temp)");
		s.executeUpdate("drop table temp");
		if((statut==1)&&(statut2==1)) {
			return true;
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return false;
}

public LinkedList<Groupe> grp_in(){
	LinkedList<Groupe> g=new LinkedList<Groupe>();
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		ResultSet rs=s.executeQuery("select * from GG where grp_on='"+this.getGroup()+"'");
		while(rs.next()) {
			Groupe gr=new Groupe();
			gr.setNom(rs.getString(2));
			g.add(gr);
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return g;
}
public boolean ajout(String id,String nom) { //Non utilisé mais permet de prendre en compte le fait qu'un étudiant qui appartient à un groupe appartient aussi
	Connection co=DBManager.getInstance().getConnection(); //au groupe dans
	try {
		Statement s=co.createStatement();
		int statut = s.executeUpdate("INSERT INTO EG(numetudiant,nom_grp) VALUES ('"+id+"','"+nom+"')");
		int statut2 = s.executeUpdate("insert into EG(numetudiant,nom_grp) select '"+id+"', grp_on from GG where grp_in='"+nom+"'");
		if ((statut==1)&&(statut2==1)) {
			return true;
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return false;
}

public LinkedList<Groupe> needaffiche(){
	LinkedList<Groupe> g=new LinkedList<Groupe>();
	Utilisateur u=(Utilisateur) request.getSession().getAttribute("user");
	Liste_Etu_Modif etudiant=new Liste_Etu_Modif(request);
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		ResultSet rs;
		rs=s.executeQuery("select * from Groupes");
		while(rs.next()) {
			Groupe grp=new Groupe();
			grp.setNom(rs.getString(1));
			grp.setDate_crea(rs.getString(2));
			grp.setProprio(rs.getString(3));
			etudiant.setNomGrp(grp.getNom());
			grp.setGrp(etudiant.etuGroupe());
			g.add(grp);
		}

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return g;
}

public boolean modifnom(String grp) {
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		String firstname=request.getParameter("groupe");
		int statut = s.executeUpdate("UPDATE Groupes SET nom='"+grp+"' where nom='"+firstname+"'");
		int statut1 = s.executeUpdate("UPDATE EG SET nom_grp='"+grp+"' where nom_grp='"+firstname+"'");
		int statut2 = s.executeUpdate("UPDATE GG SET grp_on='"+grp+"' where grp_on='"+firstname+"'");
		int statut3 = s.executeUpdate("UPDATE GG SET grp_in='"+grp+"' where grp_in='"+firstname+"'");
		return true;

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return false;
}

public boolean clonegrp(String grp) {
	Connection co=DBManager.getInstance().getConnection();
	try {
		Utilisateur u=(Utilisateur) request.getSession().getAttribute("user");
		Statement s=co.createStatement();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDate localDate = LocalDate.now();
		int statut = s.executeUpdate("Insert into Groupes(nom,date_crea,proprio) VALUES ('New"+grp+"','"+dtf.format(localDate).toString()+"','"+u.getLogin()+"')");
		ResultSet rs=s.executeQuery("Select numetudiant from EG where nom_grp='"+grp+"'");
		while(rs.next()) {
			int statut1=s.executeUpdate("Insert into EG(numetudiant,nom_grp) VALUES('"+rs.getString(1)+"','New"+grp+"')");
		}
		
		return true;

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return false;
}

public boolean ajoutunetu(String etu) {
	Connection co=DBManager.getInstance().getConnection();
	try {
		Statement s=co.createStatement();
		int statut = s.executeUpdate("INSERT INTO EG(numetudiant,nom_grp) values('"+etu+"','"+this.getGroup()+"')");
		
		
		return true;

	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	return false;
}

public static String getGroup() {
	return group;
}

public static void setGroup(String group) {
	Liste_Groupe_Modif.group = group;
}



}
