package org.groupe;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.Utilisateur;

public class CloneServlet extends  HttpServlet{ // Project_Web/clonegrp
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null && u.getAccess()>=1) {
		String pageName = "/WEB-INF/clonegrp.jsp";
		String msg=new String();
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		Liste_Groupe_Modif l=new Liste_Groupe_Modif(request);
		LinkedList<Groupe> g=l.listgg();
		request.setAttribute("groupes", g);
		String nom=request.getParameter("groupe");
		if(nom!=null) {
			if(l.clonegrp(nom)) {
				msg="Clonnage effecuté !";
			}
			else {
				msg="Déjà cloné! Veuillez renommer le groupe New"+nom+" !";
			}
			request.setAttribute("msg", msg);
			
		}
		
		
		
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			doProcess(req, resp);
		}
}
