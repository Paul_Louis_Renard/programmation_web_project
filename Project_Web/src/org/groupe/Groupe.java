package org.groupe;

import java.util.LinkedList;

import org.etudiant.Etudiant;

public class Groupe {
private LinkedList<Etudiant> grp;
private String date_crea;
private String nom;
private String proprio;
public String getDate_crea() {
	return date_crea;
}

public void setDate_crea(String date_crea) {
	this.date_crea = date_crea;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

public LinkedList<Etudiant> getGrp() {
	return grp;
}

public void setGrp(LinkedList<Etudiant> grp) {
	this.grp = grp;
}

public String getProprio() {
	return proprio;
}

public void setProprio(String proprio) {
	this.proprio = proprio;
}
}
