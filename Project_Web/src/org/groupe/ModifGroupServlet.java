package org.groupe;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.Utilisateur;

public class ModifGroupServlet extends  HttpServlet{ // Project_Web/modifnom
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null && u.getAccess()>=1) {
		String pageName = "/WEB-INF/ModifGroupe.jsp";
		String msg=new String();
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		Liste_Groupe_Modif l=new Liste_Groupe_Modif(request);
		LinkedList<Groupe> g=l.listselondroit();
		request.setAttribute("groupes", g);
		String nom=request.getParameter("name");
		if(nom!=null) {
			if(l.modifnom(nom)) {
				msg="Modification réussie !";
				g=l.listselondroit();
				request.setAttribute("groupes", g);
			}
			else {
				msg="Echec !";
			}
			request.setAttribute("msg", msg);
			
		}
		
		
		
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			doProcess(req, resp);
		}
}
