package org.groupe;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.Utilisateur;
import org.etudiant.Etudiant;
//servlet correspondant a l'url /Project_Web/deletegg
public class Deletegg extends  HttpServlet{
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null && u.getAccess()>=1) {
		String pageName = "/WEB-INF/deletegg.jsp";
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		String msg=new String();
		Liste_Groupe_Modif l=new Liste_Groupe_Modif(request);
		LinkedList<Groupe> liste=l.listselondroit();
		request.setAttribute("Liste", liste);
		String etajout=request.getParameter("grp_on");
		if(etajout!=null) {
			l.setGroup(etajout);
			LinkedList<Groupe> liste1=l.grp_in();
			request.setAttribute("Liste1", liste1);
			request.setAttribute("groupy", l.getGroup());
		}
		String nomgrp=request.getParameter("grp_in");
		if(nomgrp!=null) {
			boolean v=l.supprgg(l.getGroup(),nomgrp);
			if(v) {
				msg="Suppression : Done";
			}
			else {
				msg="Suppression : Undo";
			}
			request.setAttribute("msg", msg);
		}
		if(l.getGroup()!=null) {
			request.setAttribute("groupy", l.getGroup());
		}
		
		
		
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			doProcess(req, resp);
		}
}


