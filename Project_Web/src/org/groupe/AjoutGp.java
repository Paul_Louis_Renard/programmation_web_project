package org.groupe;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.authentification.DBManager;
import org.authentification.Utilisateur;
import org.etudiant.Etudiant;
import org.etudiant.Liste_Etu_Modif;

import com.mysql.jdbc.PreparedStatement;

public class AjoutGp {
	private HttpServletRequest request;
	
	public AjoutGp(HttpServletRequest r) {
		request=r;
	}
	
	public boolean ajout() {
		String nom=request.getParameter("Nom");
		Liste_Groupe_Modif list=new Liste_Groupe_Modif(request);
		LinkedList<Etudiant> liste=list.list();
		Liste_Etu_Modif l2 = new Liste_Etu_Modif(request);
		Connection co=DBManager.getInstance().getConnection();
		try {
			Statement s=co.createStatement();
			ResultSet rs=s.executeQuery("select * from Groupes where nom='"+nom+"'");
			if(!rs.next()) {
				Utilisateur u=(Utilisateur) request.getSession().getAttribute("user");
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
				LocalDate localDate = LocalDate.now();
				int statut = s.executeUpdate("INSERT INTO Groupes(nom,date_crea,proprio) values ('"+nom+"','"+dtf.format(localDate).toString()+"','"+u.getLogin()+"')");
				String etu = request.getParameter("listeEtu");
				statut = s.executeUpdate("INSERT INTO EG(numetudiant,nom_grp) values('"+etu+"','"+l2.getNomGrp()+"')");
				return true;
			}
			else {
				System.out.println("etu else est:"+request.getParameter("listeEtu"));
				int statut = s.executeUpdate("INSERT INTO EG(numetudiant,nom_grp) values('"+request.getParameter("listeEtu")+"','"+l2.getNomGrp()+"')");
				return false;
			}
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return false;
	}
	
}
