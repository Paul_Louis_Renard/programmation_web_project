package org.groupe;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.Utilisateur;
import org.etudiant.Etudiant;
import org.etudiant.Liste_Etu_Modif;

public class Add_grp extends  HttpServlet{
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		String pageName = "/WEB-INF/add_grp.jsp";
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null && u.getAccess()>=1) {
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		String msg=new String();
		Liste_Groupe_Modif l=new Liste_Groupe_Modif(request);
		LinkedList<Groupe> groupes=l.listselondroit();
		request.setAttribute("groupes", groupes);
		Liste_Etu_Modif l1=new Liste_Etu_Modif(request);
		
		if(request.getParameter("group")!=null) {
			l1.setNomGrp(request.getParameter("group"));
			l.setGroup(request.getParameter("group"));
			request.setAttribute("nomchoisi", l1.getNomGrp());
			LinkedList<Etudiant> listeno=l1.etuNonGroupe();
			request.setAttribute("Liste", listeno);
		}
		if(l1.getNomGrp()!=null) {
			LinkedList<Etudiant> liste=l1.etuGroupe();
			request.setAttribute("EtudiantGroupe", liste);
			LinkedList<Etudiant> listeno=l1.etuNonGroupe();
			request.setAttribute("nomchoisi", l1.getNomGrp());
			request.setAttribute("Liste", listeno);
		}
		if(request.getParameter("listeEtu")!=null) {
			if(l.ajoutunetu(request.getParameter("listeEtu"))) {
				msg="L'étudiant a été ajouté !";
				LinkedList<Etudiant> listeno=l1.etuNonGroupe();
				request.setAttribute("Liste", listeno);
				LinkedList<Etudiant> liste=l1.etuGroupe();
				request.setAttribute("nomchoisi", l1.getNomGrp());
				request.setAttribute("EtudiantGroupe", liste);
			}
			else {
				msg="Une erreur s'est produite !";
			}
			request.setAttribute("msg", msg);
		}
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			doProcess(req, resp);
		}
}


