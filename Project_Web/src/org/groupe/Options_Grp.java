package org.groupe;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.Utilisateur;
import org.etudiant.Liste_Etu_Modif;

public class Options_Grp extends HttpServlet {
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null && u.getAccess()>=1) {
		String pageName = "/WEB-INF/options_Grp.jsp";
		Liste_Etu_Modif l1=new Liste_Etu_Modif(request);
		if(request.getParameter("sup")!=null && request.getParameter("sup").equals("supprime")) {
			/**Suppression du groupe si l'utilisateur n'a pas validé sa création **/
			Liste_Groupe_Modif l2=new Liste_Groupe_Modif(request);
			l2.supprgrp(l1.getNomGrp());
			
		}
		l1.setNomGrp(null);
		
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			doProcess(req, resp);
		}
}
