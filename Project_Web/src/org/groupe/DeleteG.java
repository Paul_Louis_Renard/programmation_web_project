package org.groupe;

import java.io.IOException;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.Utilisateur;
import org.etudiant.Etudiant;
import org.etudiant.Liste_Etu_Modif;
//servlet correspondant a l'url /Project_Web/deletegrp
public class DeleteG extends  HttpServlet{
	private void doProcess(HttpServletRequest request, HttpServletResponse response) { 
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null && u.getAccess()>=1) {
		String pageName = "/WEB-INF/deleteG.jsp";
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		String msg=new String();
		Liste_Etu_Modif l1=new Liste_Etu_Modif(request);
		Liste_Groupe_Modif l=new Liste_Groupe_Modif(request);
		LinkedList<Groupe> listegg=l.listselondroit();
		request.setAttribute("Listegg", listegg);
		String etajout=request.getParameter("supp");
		String nomgrp=request.getParameter("nom");
		request.setAttribute("nomgroupe", l1.getNomGrp());
		if(nomgrp!=null) {
			l1.setNomGrp(request.getParameter("nom"));
			LinkedList<Etudiant> liste=l1.etuGroupe();
			request.setAttribute("Liste", liste);
			request.setAttribute("nomgroupe", l1.getNomGrp());
		}
		if(nomgrp!=null && etajout!=null) {
			
			boolean v=l.suppression(etajout,nomgrp);
			if(v) {
				msg="Suppression : Done";
			}
			else {
				msg="Suppression : Undo";
			}
			LinkedList<Etudiant> liste=l1.etuGroupe();
			request.setAttribute("Liste", liste);
			request.setAttribute("msg", msg);
			request.setAttribute("nomgroupe", l1.getNomGrp());
		}
		
		
		
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			doProcess(req, resp);
		}
}


