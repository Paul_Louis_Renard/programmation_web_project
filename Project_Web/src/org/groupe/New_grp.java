package org.groupe;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.authentification.DBManager;
import org.authentification.Utilisateur;
import org.etudiant.Etudiant;
import org.etudiant.Liste_Etu_Modif;

public class New_grp extends  HttpServlet{
	private void doProcess(HttpServletRequest request, HttpServletResponse response) {
		Utilisateur u=(Utilisateur)request.getSession().getAttribute("user");
		if(u!=null && u.getAccess()>=1) {
		String pageName = "/WEB-INF/new_grp.jsp";
		RequestDispatcher rd = getServletContext().getRequestDispatcher(pageName);
		String msg=new String();
		Liste_Groupe_Modif l=new Liste_Groupe_Modif(request);
		Liste_Etu_Modif l2= new Liste_Etu_Modif(request);
		String grp = request.getParameter("Nom");
		l2.setNomGrp(grp);
		System.out.println(l2.getNomGrp());
		if(l2.getNomGrp()!= null) {
			request.setAttribute("nomgroupe", l2.getNomGrp());
			if(request.getParameter("listeEtu")!=null) {
				AjoutGp ajoutgrp=new AjoutGp(request);
				boolean v=ajoutgrp.ajout();
				if(v) {
					msg="Ajout réalisé avec succès!";
				}
				else {
					msg="Le groupe existe déjà !";
				}
				request.setAttribute("msg", msg);
			}LinkedList<Etudiant> listegrp = l2.etuGroupe();
			request.setAttribute("EtudiantGroupe", listegrp);
		}
		else {
			request.setAttribute("nomgroupe", "");
		}
		LinkedList<Etudiant> liste=l.list();
		request.setAttribute("Liste", liste);
		
		
		
		
		
		try {
			rd.forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		else {
			try {
				response.sendRedirect( "/Project_Web/Connexion" );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
			doProcess(req, resp);
		}
}


