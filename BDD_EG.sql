-- --------------------------------------------------------

--
-- Structure de la table 'EG'
--

CREATE TABLE IF NOT EXISTS EG (
  numetudiant varchar(255) NOT NULL,
  nom_grp varchar(255) NOT NULL,
  FOREIGN KEY (numetudiant) REFERENCES Etudiants(numetudiant),
  FOREIGN KEY (nom_grp) REFERENCES Groupes(nom)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO EG (numetudiant,nom_grp) VALUES
('1','QuestionMark'),
('2','Nimportki'),
('3','QuestionMark'),
('4','QuestionMark'),
('5','Nimportki');


